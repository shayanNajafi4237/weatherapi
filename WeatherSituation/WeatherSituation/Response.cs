﻿namespace WeatherSituation;

public class Response
{
    public int cod { get; set; }
    public int cnt { get; set; }
    public List<List> list { get; set; }
}