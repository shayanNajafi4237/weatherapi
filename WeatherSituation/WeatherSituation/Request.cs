﻿using System;
using System.Diagnostics;
using System.Net.Http;
using Newtonsoft.Json;

namespace WeatherSituation;

public class Request
{
    public static string apiKey = "766ef6d4f9834f0f5ddaae17aba256f2";
    public static int latitude = 35;
    public static int longtitude = 51;
    public static string urlConst = "https://api.openweathermap.org/data/2.5/forecast";
    public static string url;

    public Request()
    {
        url = urlConst + "?lat=" + latitude.ToString() + "&lon=" + longtitude.ToString() +
              "&appid=" + apiKey;
        SendRequest();
    }


    public async void SendRequest()
    {
        using (HttpClient client = new HttpClient())
        {
            using (HttpResponseMessage response = await client.GetAsync(url))
            {
                using (HttpContent content = response.Content)
                {
                    string mycontent = await content.ReadAsStringAsync();
                    DeserializeJson(mycontent);
                }
            }
        }
    }

    public void DeserializeJson(string jsonText)
    {
        Response weatherInformation = JsonConvert.DeserializeObject<Response>(jsonText);
        GetAverage(weatherInformation);
    }

    public void GetAverage(Response resopnseObject)
    {
        List<float> averageTemp = new List<float>(6);
        float sumOfTempInOneDay = 0;
        int numOfRecordsInOneDay = 0;
        int lastDayOfInfo = DateTime.Parse(resopnseObject.list[0].dt_txt).Day;

        foreach (var info in resopnseObject.list)
        {
            int day = DateTime.Parse(info.dt_txt).Day;
            if (day == lastDayOfInfo)
            {
                sumOfTempInOneDay += info.main.temp;
                numOfRecordsInOneDay++;
            }
            else
            {
                averageTemp.Add(sumOfTempInOneDay / numOfRecordsInOneDay);
                sumOfTempInOneDay = info.main.temp;
                numOfRecordsInOneDay = 1;
            }

            lastDayOfInfo = day;
        }

        averageTemp.Add(sumOfTempInOneDay / numOfRecordsInOneDay);

        foreach (var temp in averageTemp)
        {
            Console.WriteLine(temp);
        }
    }
}